<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" name="viewport">
	<title>Petlife</title>
	<style type="text/css">
.ReadMsgBody { width: 100%; background-color: #ffffff; }
.ExternalClass { width: 100%; background-color: #ffffff; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
html { width: 100%; }
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
table { border-spacing: 0;border-collapse: collapse; }
img { display: block !important; }
table td { border-collapse: collapse; }
.yshortcuts a { border-bottom: none !important; }
a { color: #00C4B4; text-decoration: none; }
.textbutton a { font-family: 'open sans', arial, sans-serif !important; color: #ffffff !important; }
hr { margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eee; }
 @media only screen and (max-width: 640px) {
body { width: auto !important; }
table[class="table600"] { width: 450px !important; text-align: center !important; }
table[class="table-inner"] { width: 86% !important; text-align: center !important; }
table[class="table2-2"] { width: 47% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 29.9% !important; }
table[class="table3-1"] { width: 64% !important; text-align: center !important; }
table[class="footer-note"] { width: 100% !important; text-align: left !important; }
table[class="footer-column"] { width: 47% !important; text-align: left !important; }
/* Image */
img[class="img1"] { width: 100% !important; height: auto !important; }
}
 @media only screen and (max-width: 479px) {
body { width: auto !important; }
table[class="table600"] { width: 290px !important; }
table[class="table-inner"] { width: 82% !important; }
table[class="table2-2"] { width: 100% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 100% !important; }
table[class="table3-1"] { width: 100% !important; text-align: center !important; }
table[class="footer-note"] { width: 100% !important; text-align: center !important; }
table[class="footer-column"] { width: 100% !important; text-align: center !important; }
/* image */
img[class="img1"] { width: 100% !important; }
}
</style>

</head>

<body style="background: #f1f1f1;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">

		<!--header-2-->
		<tbody><tr>
			<td>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
					<tbody><tr>
						<td valign="top" bgcolor="#00C4B4" background="" height="250" style=" background-size:cover;background-position:center; background-position:top;">
							<!--[if gte mso 9]>
							<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
								<v:fill type="tile" src="http://farm4.staticflickr.com/3754/12079806886_9a9a7cd2f7_o.jpg" color="#e8aa9b" />
								<v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
									<![endif]-->

									<div>
										<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" height="250" class="table600">
											<tbody><tr>
												<td align="center" height="40"></td>
											</tr>
											<tr>
												<td align="center">
								<a href=""><img alt="img" src="http://webmamu.com/petlife/public/site/images/logo-lg.png"></a>
												</td>
											</tr>
											<tr>
												<td height="20"></td>
											</tr>
											<tr>
												
											</tr>
											<tr>
												<td height="30"></td>
											</tr>
										</tbody></table>
									</div>

									<!--[if gte mso 9]>
								</v:textbox>
							</v:rect>
							<![endif]-->
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<!--end header-2-->

		<!-- 1/1 content -->
		
		<!-- end 1/1 content -->

		<!-- Title bar -->
		<tr>
			<td style="background: #f1f1f1;">
				<table cellspacing="0" cellpadding="0" border="0" align="center" class="table600" style="max-width: 600px;">
					<tbody>
					
					
					
					
<tr align="left"><td style="height: 50px;"></td></tr>
<tr align="left">
						<td style="margin-top: 0px; margin-bottom: 10px; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; color: rgb(67, 67, 67); font-weight: 200; font-size: 20px; line-height: 20px; display: block; padding: 20px 40px; background: none repeat scroll 0% 0% rgb(255, 255, 255); border-radius: 6px; border: 1px solid rgba(0, 0, 0, 0.1); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
                        <div width="560px" style="font-size: 14px;">
                          <p style="border-bottom: 2px dotted rgb(247, 246, 244); width: auto;"><strong style="font-weight: normal; font-family: Georgia,serif; margin: 0px; line-height: 40px; font-size: 18px; display: block; padding: 0px 0px;">Welcome to Petlife.com </strong></p>
                          <p><strong>Hi, {{$data['first_name']}}</strong></p>
                          <p>Thanks for booking in Petlife.com. </p>
                          <p><strong>Email:</strong>  <a href="#mailTo">melonbscheme@gmail.com</a></p>
                          <p>
                          	Before you proceed, we recommand you to verify your email by clicking <a href="#">this link</a>
                          </p>
                          <p>
                          	Thank you,<br />Petlife.com Team 
                          </p>
                          <hr >
                          
                          <p>
                          	Petlife.com sent this message to sk <a href="#">(petlife@bscheme.com)</a>
                          </p>
                          <p>
                          	Petlife.com is committed to your privacy. Learn more about our <a href="#">privacy policy</a> and <a href="#">terms</a>
                          </p>
                          
                          
                        </div>
                        </td>
					</tr>
                    
				</tbody></table>
			</td>
		</tr>
		<!-- end title bar -->
		<!-- 1/2 panel [right text] -->
		

		
		
		<tr style="margin-top: 20px; background: #f1f1f1;"><td>
							<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="table600">
								<tbody><tr>
									<td height="10"></td>
								</tr>
								<tr>
									<td valign="middle" height="30" style="font-size: 12px; color: rgb(139, 136, 131); font-family: arial; line-height: 15px; padding-bottom: 20px; text-align: center; padding-top: 0px;">
													Copyright &copy;
													<a href="http://slapdab.com" style="color:#00C4B4 !important"> Petlife.com</a>
													, All rights reserved
                                                    <br><span style="width: 471px; color: rgb(102, 102, 102); font-family: arial; font-size: 10px;">Powered by <a style="color:#00C4B4" href="" target="_blank">Bluescheme</a></span>
												</td>
								</tr>
								<tr>
									<td height="10"></td>
								</tr>
							</tbody></table>
						</td></tr>

	</tbody></table>

</body></html>